describe Transport do
    describe "#get_eta" do
        context "get estimation of time arrival" do
            it "returns 2" do
                let(:speed) { 30 } 
                let(:max_weight) { 20 } 
                let(:available) { true } 
                let(:transport) { described_class.new(speed, max_weight, available) }
                expect(:transport1.get_eta(40)).to eq(2)
            end
        end
    end
end
