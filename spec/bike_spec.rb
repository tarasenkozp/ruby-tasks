describe Bike do
    describe "#initialize" do
        context "create object of Bike" do
            it "return kind of bike" do
                let(:bike) { described_class.new }
                expect(:bike).to be_kind_of(Bike)
            end
        end
    end
end
