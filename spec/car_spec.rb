describe Car do
    describe "#initialize" do
        context "create object of Car" do
            it "return kind of bike" do
                let(:car) { described_class.new('0000') }
                expect(:car).to be_kind_of(Car)
            end
        end
    end
end
