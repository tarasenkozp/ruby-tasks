require 'rspec'
require 'simplecov'
SimpleCov.start

require_relative "../lib/index.rb"
require_relative "mocks.rb"
require_relative "car_spec.rb"
require_relative "bike_spec.rb"
require_relative "delivery_spec.rb"
require_relative "transport_spec.rb"
