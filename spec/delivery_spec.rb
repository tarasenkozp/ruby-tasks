describe Delivery do
    describe "#check_vehicles" do
        context "search available vehicle for delivery" do
            it "return kind of Bike" do
                let(:delivery) { described_class.new(BIKES) }
                expect(:delivery.check_vehicles(WEIGHT, DISTANCE)[:vehicle]).to be_kind_of(Bike)
            end

            it "return kind of Car" do
                let(:delivery) { described_class.new(CARS) }
                expect(:delivery.check_vehicles(WEIGHT, DISTANCE)[0]).to be_kind_of(Car)
            end
        end
    end
end
