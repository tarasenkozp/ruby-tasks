WEIGHT = 10
DISTANCE = 40
VEHICLES = [
  Bike.new(true),
  Bike.new(true),
  Bike.new(false),
  Bike.new(true),
  Bike.new(true),
  Car.new('0000', false),
  Car.new('1111', false),
  Car.new('2222', false),
  Car.new('3333', true),
  Car.new('4444', false),
  Car.new('5555', false)
]
