require_relative 'transport'

class Bike < Transport
  attr_reader :max_distance
  attr_accessor :is_available

  def initialize(is_available)
    super()
    @speed = 10
    @max_weight = 10
    @max_distance = 30
    @delivery_cost = 10
    @location = 'In garage'
    @number_of_deliveries = 0
    @is_available = is_available
  end
end
