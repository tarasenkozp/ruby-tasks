require_relative 'car'
require_relative 'bike'

class Delivery
  include Comparable
  def initialize(vehicles)
    @vehicles = vehicles
  end

  def check_vehicles(weight, distance)
    @vehicles.each do |vehicle|
      is_available = vehicle.is_available
      is_weight = check_weight(vehicle, weight)
      is_distance = check_distance(vehicle, distance)
      is_delivered = check_delivery()
      vehicle_id = nil

      if vehicle.instance_of? Car
        vehicle_id = vehicle.id
      end

      if is_available && is_weight && is_distance
        return {
          :vehicle => vehicle,
          :vehicle_id => vehicle_id,
          :is_delivered => is_delivered,
          :eta => vehicle.get_eta(distance)
        }
      end
    end
  end

  private

  def check_weight(vehicle, weight)
    vehicle.max_weight >= weight
  end

  def check_distance(vehicle, distance)
    if vehicle.respond_to? :max_distance
      vehicle.max_distance < distance
    else
      false
    end
  end

  def check_delivery
    true
  end
end
