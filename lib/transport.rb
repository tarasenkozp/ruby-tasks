class Transport
  attr_accessor :max_weight, :speed, :is_available, :location, :number_of_deliveries, :delivery_cost

  def self.all
    ObjectSpace.each_object(self).to_a
  end

  def get_eta(distance)
    distance.to_f / @speed
  end

  def self.find_by_max_weight(condition)
    all.detect { |instance| instance.max_weight == condition }
  end

  def self.find_by_speed(condition)
    all.detect { |instance| instance.speed == condition }
  end

  def self.find_by_is_available(condition)
    all.detect { |instance| instance.is_available == condition }
  end

  def self.find_by_location(condition)
    all.detect { |instance| instance.location == condition }
  end

  def self.find_by_number_of_deliveries(condition)
    all.detect { |instance| instance.number_of_deliveries == condition }
  end

  def self.find_by_delivery_cost(condition)
    all.detect { |instance| instance.delivery_cost == condition }
  end

  def self.filter_by_max_weight
    all.select { |instance| yield(instance.max_weight) }
  end

  def self.filter_by_speed
    all.select { |instance| yield(instance.speed) }
  end

  def self.filter_by_is_available
    all.select { |instance| yield(instance.is_available) }
  end

  def self.filter_by_location
    all.select { |instance| yield(instance.location) }
  end

  def self.filter_by_delivery_cost
    all.select { |instance| yield(instance.delivery_cost) }
  end

  def self.filter_by_number_of_deliveries
    all.select { |instance| yield(instance.number_of_deliveries) }
  end
end
