# frozen_string_literal: true

require_relative 'delivery'
require_relative 'constants'
require_relative 'car'
require_relative 'bike'

def init
  delivery = Delivery.new(VEHICLES)
  delivery.check_vehicles(WEIGHT, DISTANCE)
end

def show_delivery_info(data)
  if data[:is_delivered] != false
    puts 'delivery status: delivered'
  else
    puts 'delivery status: in progress'
  end

  puts "estimation time arrival: #{data[:eta]} hours" unless data[:eta].nil?

  if !data[:vehicle_id].nil?
    puts 'vehicle type: car'
  else
    puts 'vehicle type: bike'
  end

  puts "transport: #{data[:vehicle_id]}" unless data[:vehicle_id].nil?
end

init
# init_delivery_info = init
# show_delivery_info(delivery_info)

all_cars = Car.all
find_cars_available = Car.find_by_is_available(true)
filter_cars_available = Car.filter_by_is_available { |item| item == true }
find_cars_un_available = Car.find_by_is_available(false)
filter_cars_un_available = Car.filter_by_is_available { |item| item == false }

puts '-----------------------------------'
puts 'CARS'
puts '-----------------------------------'
puts 'all'
puts all_cars
puts '-----------------------------------'
puts 'find available'
puts find_cars_available
puts '-----------------------------------'
puts 'filter available'
puts filter_cars_available
puts '-----------------------------------'
puts 'find unavailable'
puts find_cars_un_available
puts '-----------------------------------'
puts 'filter unavailable'
puts filter_cars_un_available
puts '-----------------------------------'

all_bikes = Bike.all
find_bikes_available = Bike.find_by_is_available(true)
filter_bikes_available = Bike.filter_by_is_available { |item| item == true }
find_bikes_un_available = Bike.find_by_is_available(false)
filter_bikes_un_available = Bike.filter_by_is_available { |item| item == false }

puts 'BIKES'
puts '-----------------------------------'
puts 'all'
puts all_bikes
puts '-----------------------------------'
puts 'find available'
puts find_bikes_available
puts '-----------------------------------'
puts 'filter available'
puts filter_bikes_available
puts '-----------------------------------'
puts 'find unavailable'
puts find_bikes_un_available
puts '-----------------------------------'
puts 'filter unavailable'
puts filter_bikes_un_available
puts '-----------------------------------'
