require_relative 'transport'

class Car < Transport
  attr_reader :id
  attr_writer :is_available

  def initialize(id, is_available)
    super()
    @id = id
    @speed = 50
    @max_weight = 100
    @delivery_cost = 100
    @location = 'In garage'
    @number_of_deliveries = 0
    @is_available = is_available
  end
end
